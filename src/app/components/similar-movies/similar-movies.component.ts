import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import Swiper from 'swiper';
import { Router } from '@angular/router';
import { Results } from '../../interfaces/similar-response';

@Component({
  selector: 'app-similar-movies',
  templateUrl: './similar-movies.component.html',
  styleUrls: ['./similar-movies.component.css']
})
export class SimilarMoviesComponent implements OnInit, AfterViewInit {

  @Input() results: Results[];

  constructor(  private router: Router ) { }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    const swiper = new Swiper('.swiper-container', {
      slidesPerView: 5.3,
      freeMode: true,
      spaceBetween: 15
    });
  }

  onMovieClick( results: Results ) {
    this.router.navigate(['/pelicula', results.id ]);
  }

}
